export type AbilityName =
  | "Brawl"
  | "Single"
  | "Great"
  | "Thrown"
  | "Bow"
  | "Crossbow";
