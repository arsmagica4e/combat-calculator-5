import { Cost } from "./cost";

export interface Armor {
  material: string;
  cost: Cost;
  coverage: "Partial" | "Full";
  protection: number;
  load: number;
}

export const ArmorTable: Armor[] = [
  {
    material: "None",
    cost: "n/a",
    coverage: "Partial",
    protection: 0,
    load: 0,
  },
  {
    material: "Quilted/Fur",
    cost: "Inexpensive",
    coverage: "Partial",
    protection: 1,
    load: 2,
  },
  {
    material: "Heavy Leather",
    cost: "Inexpensive",
    coverage: "Partial",
    protection: 2,
    load: 3,
  },
  {
    material: "Metal Reinforced Leather",
    cost: "Standard",
    coverage: "Partial",
    protection: 2,
    load: 2,
  },
  {
    material: "Metal Reinforced Leather",
    cost: "Standard",
    coverage: "Full",
    protection: 4,
    load: 4,
  },
  {
    material: "Leather Scale",
    cost: "Standard",
    coverage: "Partial",
    protection: 3,
    load: 3,
  },
  {
    material: "Leather Scale",
    cost: "Standard",
    coverage: "Full",
    protection: 5,
    load: 5,
  },
  {
    material: "Metal Scale",
    cost: "Standard",
    coverage: "Partial",
    protection: 4,
    load: 4,
  },
  {
    material: "Metal Scale",
    cost: "Standard",
    coverage: "Full",
    protection: 7,
    load: 7,
  },
  {
    material: "Chain Mail",
    cost: "Expensive",
    coverage: "Partial",
    protection: 6,
    load: 4,
  },
  {
    material: "Chain Mail",
    cost: "Expensive",
    coverage: "Full",
    protection: 9,
    load: 6,
  },
];

export const armorLabel = (a: Armor): string => {
  return `${a.material} - ${a.coverage} p:${a.protection} l:${a.load} (${a.cost})`
}
