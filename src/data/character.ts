import { AbilityName } from "./AbilityName";
import { ArmorTable } from "./armor";
import { Weapon, WeaponsTable } from "./weapons";

export interface Fatigue {
  name: string;
  penalty: number;
  recovery: string;
}

export const Fatigues: Fatigue[] = [
  {name: 'Fresh', penalty: 0, recovery: "-"},
  {name: 'Winded', penalty: 0, recovery: "2m"},
  {name: 'Weary', penalty: -1, recovery: "10m"},
  {name: 'Tired', penalty: -3, recovery: "30m"},
  {name: 'Dazed', penalty: -5, recovery: "1h"},
  {name: 'Unconcious', penalty: -Infinity, recovery: "2h"}
]

export type CharacteristicShort = "Int" | "Per" | "Str" | "Sta" | "Pre" | "Com" | "Dex" | "Qik"

export interface Ability {
  name: AbilityName;
  score: number;
  specialization: string;
}

export interface Characteristic {
  name: string;
  short: CharacteristicShort;
  description?: string;
  score: number;
}

export interface Character {
  name: string;
  abilities: Ability[];
  characteristics: Record<CharacteristicShort, Characteristic>;
  size: number;
  load: number;
  tough: boolean;
  weapons: Weapon[];
  armor: number;
  fatigue: number;
  wounds: {
    light: number;
    medium: number;
    heavy: number;
    incapacitated: boolean;
    dead : boolean;
  };
  showSelectedWeapons: boolean;
}

export const DefaultCharacter: Character = {
  name: '',
  characteristics: {
    Int: {
      name: "Intelligence",
      short: "Int",
      score: 0,
    },
    Per: {
      name: "Perception",
      short: "Per",
      score: 0,
    },
    Str: {
      name: "Strength",
      short: "Str",
      score: 0,
    },
    Sta: {
      name: "Stamina",
      short: "Sta",
      score: 0,
    },
    Pre: {
      name: "Presence",
      short: "Pre",
      score: 0,
    },
    Com: {
      name: "Communication",
      short: "Com",
      score: 0,
    },
    Dex: {
      name: "Dexterity",
      short: "Dex",
      score: 0,
    },
    Qik: {
      name: "Quickness",
      short: "Qik",
      score: 0,
    }
  },

  size: 0,
  load: 0,

  weapons: [WeaponsTable[0]],
  armor: 0, //ArmorTable[0],
  tough: false,

  abilities: [...new Set(WeaponsTable.map((w) => w.skill))]
    .map((n) => ({ name: n, score: 0, specialization: "null" })),

  fatigue: 0,
  wounds: {
    light: 0,
    medium: 0,
    heavy: 0,
    incapacitated: false,
    dead: false
  },
  showSelectedWeapons: false,
}

export function getAbility(c: Character, w: Weapon): number {
  let bonus = 0;
  const ab = c.abilities.find(a => a.name == w.skill)
  bonus += ab.score
  bonus += ab.specialization === w.name ? 1 : 0
  return bonus;
}

function burden(load: number): number {
  const cutoffs = [0,1,3,6,10,15,21,28,36,45,55]
  return cutoffs.findIndex(c => (load <= c))
}
export function load(c: Character): number {
  return c.weapons.reduce((t:number, w:Weapon):number => t + (isNaN(w.load)?0:w.load), ArmorTable[c.armor].load)
}
export function encumbrance(c: Character): number {
  return Math.max(burden(Math.floor(load(c))) - c.characteristics.Str.score, 0)
}
export function soak(c: Character): number {
  return c.characteristics.Sta.score + ArmorTable[c.armor].protection + (c.tough?3:0)
}
export function penalty(c: Character): number {
  if (c.wounds.incapacitated || c.wounds.dead || c.fatigue >= 5) return -Infinity
  return Fatigues[c.fatigue].penalty -c.wounds.light - (c.wounds.medium * 3) - (c.wounds.heavy * 5)
}

export function hasShield(c: Character): Weapon {
  const shields = c.weapons.filter(w => w.name.match(/Shield/))
  if (shields) {
    shields.sort((a, b) => b.dfn - a.dfn)
    return shields[0]
  }
  return null
}