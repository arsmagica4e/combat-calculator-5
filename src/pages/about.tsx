import * as React from "react";
import { Header, Container, Segment, Icon } from "semantic-ui-react";
import {withLayout} from "../components/Layout";

const AboutPage = () => {
  return (
    <Container>
      <Segment vertical>
        <Header as="h2">
          <Icon name="info circle" />
          <Header.Content>
            About
          </Header.Content>
        </Header>
      </Segment>
      <Segment vertical>
        <p>
	  Authored by @mringenb.
	</p>
        <p>
          For any issues, any PR are welcomed
          <a href="https://gitlab.com/arsmagica4e/combat-calculator-5/issues" target="blank"> on this repository</a>
        </p>
      </Segment>
    </Container>
  );
};

export default withLayout(AboutPage);
