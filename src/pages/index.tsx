import * as React from "react";
import {
  Container,
  Header, Segment
} from "semantic-ui-react";
import CharacterSheet from "../components/CharacterSheet/CharacterSheet";
//import HeaderMenu from "../components/HeaderMenu/HeaderMenu";
import { LayoutProps, withLayout } from "../components/Layout";

const IndexPage = (props: LayoutProps) =>
  <div>
    {/* Master head */}
    <Segment vertical inverted textAlign="center" className="xmasthead">
      <Container text>
        <Header inverted as="h1">Ars Magica 5th Edition Derived Combat Calculator</Header>
        <Header inverted as="h2">Michael Ringenberg</Header>
      </Container>
    </Segment>

    {/*<ThemeToggler>
      {({ theme, toggleTheme }) => (
        <Checkbox toggle label="Dark Mode" checked={theme === 'dark'}
        onChange={(e, data) => toggleTheme(data.checked ? 'dark' : 'light')}></Checkbox>
      )}
      </ThemeToggler> not working */}
    {/* Character Sheet */}
    <CharacterSheet></CharacterSheet>
  </div>;

export default withLayout(IndexPage);
