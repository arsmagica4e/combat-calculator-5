import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Characteristic from "./Characteristic";

/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const CharacteristicReadme = require("./README.md");

storiesOf("Characteristic", module)
  .addDecorator(withReadme(CharacteristicReadme))
  .add("default", () => {
    const characteristic: Characteristic = null; // TODO assign real value

    return (
      <Characteristic characteristic={characteristic} />
    );
  });
