import * as React from "react";
import { Grid } from "semantic-ui-react";
import { Characteristic } from "../../data/character";

interface AttributeProps extends React.HTMLProps<HTMLDivElement> {
  characteristic: Characteristic;
  onChangeCharacteristic: (evt: React.ChangeEvent<HTMLInputElement>) => void;
}

export default (props: AttributeProps) => {
  const char = props.characteristic
  return (
    <Grid.Row>
      <Grid.Column width={2}>
        <label>{char.name}</label>
      </Grid.Column>
      <Grid.Column>
        <span>{char.short}</span>
      </Grid.Column>
      <Grid.Column>
        <input type="number"
          name={char.short}
          value={props.characteristic.score}
          min="-5" max="5" step="1"
          onChange={props.onChangeCharacteristic} />
      </Grid.Column>
    </Grid.Row>
  )
}
