import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import Characteristic from "./Characteristic";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("Characteristic component", () => {
  it("should render correctly", () => {
    const characteristic: Characteristic = null; // TODO assign real value

    const wrapper = render(<Characteristic characteristic={characteristic} />);
    expect(wrapper).toMatchSnapshot();
  });
});
