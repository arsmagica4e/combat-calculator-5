import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import WeaponEntry from "./WeaponEntry";

/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const WeaponEntryReadme = require("./README.md");

storiesOf("WeaponEntry", module)
  .addDecorator(withReadme(WeaponEntryReadme))
  .add("default", () => {
    const weapon: Weapon = null; // TODO assign real value

    return (
      <WeaponEntry weapon={weapon} />
    );
  });
