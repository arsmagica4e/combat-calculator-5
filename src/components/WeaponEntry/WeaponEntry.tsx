import * as React from "react";
import { Checkbox } from "semantic-ui-react";
import { Character, encumbrance, getAbility, hasShield, penalty } from "../../data/character";
import { Weapon, WeaponsTable } from "../../data/weapons";

interface WeaponEntryProps extends React.HTMLProps<HTMLDivElement> {
  weapon: Weapon;
  character: Character;
  onCheck: (evt: React.ChangeEvent<HTMLInputElement>) => void;
}

const WeaponEntry = (props: WeaponEntryProps): JSX.Element => {
  const str = props.character.characteristics.Str.score;
  const dex = props.character.characteristics.Dex.score;
  const qik = props.character.characteristics.Qik.score;
  const ability = getAbility(props.character,props.weapon);
  const enc = encumbrance(props.character);
  const weaponIndex = WeaponsTable.findIndex(w => props.weapon===w)
  //const checked = props.character.weapons.some(w => w.name==props.weapon.name)
  //console.log(weaponIndex, checked)
  let shielded = 0
  const wound = penalty(props.character)
  if (props.weapon.skill === "Single" && !props.weapon.name.match(/Shield/)) {
    const shield = hasShield(props.character)
    shielded += shield ? shield.dfn : 0
  }
  const isDodge = props.weapon.name === 'Dodge'
  return (
    <tr>
      {/* Weapon */}
      <td><Checkbox className="weapon-checkbox" disabled={isDodge} onChange={props.onCheck} value={weaponIndex} label={props.weapon.name} checked={props.character.weapons.some(w => w.name==props.weapon.name)}></Checkbox></td>
      {/* INIT = Qik+Weap-Enc */}
      <td className="weapon-data-col">{qik + props.weapon.init - enc}</td>
      {/* ATK = Dex+Ability+Weap */}
      <td className="weapon-data-col">{isNaN(props.weapon.atk) ? '-' : (dex + ability + props.weapon.atk + wound)}</td>
      {/* DFN = Qik+Ability+Weap */}
      <td className="weapon-data-col">{qik + ability + props.weapon.dfn + shielded + wound}</td>
      {/* DAM = Str+Weap */}
      <td className="weapon-data-col">{isNaN(props.weapon.dam) ? '-' : (str + props.weapon.dam)}</td>
      {/* Load */}
      <td className="weapon-data-col">{isNaN(props.weapon.load) ? '-' : props.weapon.load}</td>
      {/* Range */}
      <td className="weapon-data-col">{props.weapon.range??'-'}</td>
    </tr>
  );
};
WeaponEntry.displayName = 'WeaponEntry'
export default WeaponEntry
