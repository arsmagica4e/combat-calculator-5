import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import WeaponEntry from "./WeaponEntry";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("WeaponEntry component", () => {
  it("should render correctly", () => {
    const weapon: Weapon = null; // TODO assign real value

    const wrapper = render(<WeaponEntry weapon={weapon} />);
    expect(wrapper).toMatchSnapshot();
  });
});
