import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import Ability from "./Ability";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("Ability component", () => {
  it("should render correctly", () => {
    const ability: string = null; // TODO assign real value

    const wrapper = render(<Ability ability={ability} />);
    expect(wrapper).toMatchSnapshot();
  });
});
