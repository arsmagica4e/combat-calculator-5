import * as React from "react";
import { Grid } from "semantic-ui-react";
import { WeaponsTable } from "../../data/weapons";
import { Ability } from '../../data/character';

interface AbilityProps extends React.HTMLProps<HTMLDivElement> {
  ability: Ability;
  onChangeSpecialization: (evt: React.ChangeEvent<HTMLSelectElement>) => void;
  onChangeAbility: (evt: React.ChangeEvent<HTMLInputElement>) => void;
}

export default (props: AbilityProps) => {
  const skill = props.ability.name
  const specializations = WeaponsTable.filter((weapon) => weapon.skill === skill).map(
    (weapon) => (
      <option key={`${skill}_${weapon.name}`} value={weapon.name}>
        {weapon.name}
      </option>
    )
  )
  return (
    <Grid.Row>
      <Grid.Column key={`${skill}_name`}>
        <label>{skill}</label>
      </Grid.Column>
      <Grid.Column key={`${skill}_spec`}>
        <select name={skill}
          onChange={props.onChangeSpecialization}
          value={props.ability.specialization}>
          <option value="null">----</option>
          {specializations}
        </select>
      </Grid.Column>
      <Grid.Column key={`${skill}_val`}>
        <input type="number"
          name={skill}
          value={props.ability.score}
          min="0" step="1"
          onChange={props.onChangeAbility}
        />
      </Grid.Column>
    </Grid.Row>
  )
}
