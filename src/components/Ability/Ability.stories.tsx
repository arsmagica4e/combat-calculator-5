import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Ability from "./Ability";

/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const AbilityReadme = require("./README.md");

storiesOf("Ability", module)
  .addDecorator(withReadme(AbilityReadme))
  .add("default", () => {
    const ability: string = null; // TODO assign real value

    return (
      <Ability ability={ability} />
    );
  });
