import * as React from "react";
import { Button, Checkbox, Container, Dropdown, Grid, Input, Item, Menu, Segment, Table } from "semantic-ui-react";
import { armorLabel, ArmorTable } from "../../data/armor";
import { Character, CharacteristicShort, DefaultCharacter, encumbrance, Fatigues, load, soak } from "../../data/character";
import { WeaponsTable } from "../../data/weapons";
import WeaponAbility from "../Ability/Ability";
import Attribute from "../Characteristic/Characteristic";
import WeaponEntry from "../WeaponEntry/WeaponEntry";

export default class CharacterSheet extends React.Component<Record<string, unknown>, Character> {
  state: Character = DefaultCharacter;
  exportLink = React.createRef<HTMLAnchorElement>();
  importLink = React.createRef<HTMLInputElement>();

  onChangeName(evt: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({ name: evt.target.value })
  }
  onChangeCharacteristic(evt: React.ChangeEvent<HTMLInputElement>): void {
    const score = evt.target.valueAsNumber
    const short = evt.target.name as CharacteristicShort
    const characteristics = this.state.characteristics
    characteristics[short].score = score
    this.setState({ characteristics: characteristics })
  }
  onChangeAbility(evt: React.ChangeEvent<HTMLInputElement>): void {
    const score = evt.target.valueAsNumber
    const name = evt.target.name
    const i = this.state.abilities.findIndex(a => a.name === name)
    const abilities = this.state.abilities.slice()
    abilities[i].score = score
    this.setState({ abilities: abilities })
  }
  onChangeSpecialization(evt: React.ChangeEvent<HTMLSelectElement>): void {
    const spec = evt.target.value
    const name = evt.target.name
    const i = this.state.abilities.findIndex(a => a.name === name)
    const abilities = this.state.abilities.slice()
    abilities[i].specialization = spec
    this.setState({ abilities: abilities })
  }
  onChangeArmor(evt: React.ChangeEvent<HTMLSelectElement>): void {
    const i = +evt.target.value
    this.setState({ armor: i })
  }
  onCheckWeapon(evt: React.ChangeEvent<HTMLInputElement>): void {
    const weap = +(evt.target.parentNode.firstChild as HTMLInputElement).value
    const weapon = WeaponsTable[weap]
    let weapons = this.state.weapons.slice()
    if (!weapons.find(w => w === weapon)) {
      weapons.push(weapon)
    } else {
      weapons = weapons.filter(w => w !== weapon)
    }
    this.setState({ weapons: weapons })
  }
  onTough(): void {
    this.setState({ tough: !this.state.tough })
  }
  onChangeSize(evt: React.ChangeEvent<HTMLInputElement>): void {
    const size = +evt.target.value
    this.setState({ size: size })
  }
  onChangeFatigue(evt: React.ChangeEvent<HTMLSelectElement>): void {
    this.setState({ fatigue: +evt.target.value })
  }
  onChangeLight(evt: React.ChangeEvent<HTMLInputElement>): void {
    const wounds = this.state.wounds
    wounds.light = +evt.target.value
    this.setState({ wounds })
  }
  onChangeMedium(evt: React.ChangeEvent<HTMLInputElement>): void {
    const wounds = this.state.wounds
    wounds.medium = +evt.target.value
    this.setState({ wounds })
  }
  onChangeHeavy(evt: React.ChangeEvent<HTMLInputElement>): void {
    const wounds = this.state.wounds
    wounds.heavy = +evt.target.value
    this.setState({ wounds })
  }
  onChangeIncap(evt: React.ChangeEvent<HTMLInputElement>): void {
    const wounds = this.state.wounds
    wounds.incapacitated = evt.target.checked
    this.setState({ wounds })
  }
  onChangeDead(evt: React.ChangeEvent<HTMLInputElement>): void {
    const wounds = this.state.wounds
    wounds.dead = evt.target.checked
    this.setState({ wounds })
  }
  onChangeShowSelectWeapons(): void {
    this.setState({ showSelectedWeapons: !this.state.showSelectedWeapons })
  }
  onExport(): void {
    const data = `data:text/json;charset=utf8,${encodeURIComponent(JSON.stringify(this.state))}`
    this.exportLink.current.setAttribute("href", data)
    const fn = this.state.name ?? 'character'
    this.exportLink.current.setAttribute('download', `${fn}.json`)
    this.exportLink.current.click()
  }
  onImport(evt: React.ChangeEvent<HTMLInputElement>): void {
    const file: File = evt.target.files[0]
    file.text().then((text: string) => JSON.parse(text) as Character)
      .then(char => this.setState(char))
      .catch(() => alert('Malformed character'))
  }
  onSave(): void {
    const key = `am5cc_${this.state.name}`
    localStorage.setItem(key, JSON.stringify(this.state))
  }
  onLoad(key: string): void {
    const file = localStorage.getItem(key)
    if (file) {
      try {
        const loaded = JSON.parse(file) as Character
        this.setState(loaded)
        /*loaded.weapons.forEach(weapon => {
          const index = WeaponsTable.findIndex((wte) => wte.name === weapon.name)
          console.log(index)
          // find checkbox whose value is index
          // add checked
        })*/
      } catch (err) {
        alert(`Unable to load ${key}: ${err}`)
      }
    }
  }
  render(): React.ReactNode {
    const names: JSX.Element[] = []
    if (typeof window !== 'undefined') {
      for (let i = 0; i < window.localStorage.length; i++) {
        const key = localStorage.key(i);
        if (key.startsWith('am5cc_')) {
          names.push(<Dropdown.Item key={key} onClick={() => this.onLoad(key)} text={key.slice(6)} />)
        }
      }
    }

    const int = this.state.characteristics['Int'];
    const per = this.state.characteristics['Per'];
    const str = this.state.characteristics['Str'];
    const sta = this.state.characteristics['Sta'];
    const pre = this.state.characteristics['Pre'];
    const com = this.state.characteristics['Com'];
    const dex = this.state.characteristics['Dex'];
    const qik = this.state.characteristics['Qik'];
    const characteristics = [int, per, str, sta, pre, com, dex, qik].map((c) => (
      <Attribute key={c.short} characteristic={c} onChangeCharacteristic={this.onChangeCharacteristic.bind(this)}></Attribute>
    ))
    const abilities = this.state.abilities.map(a => (
      <WeaponAbility key={a.name} ability={a} onChangeAbility={this.onChangeAbility.bind(this)} onChangeSpecialization={this.onChangeSpecialization.bind(this)}></WeaponAbility>
    ))
    const weapons = WeaponsTable.filter(weap => !this.state.showSelectedWeapons || this.state.weapons.indexOf(weap) >= 0).map(w => (
      <WeaponEntry key={`${w.name}_${w.skill}`} weapon={w} character={this.state} onCheck={this.onCheckWeapon.bind(this)}></WeaponEntry>))
    const armors = ArmorTable.map((a, i) => (
      <option key={`${a.material}_${a.coverage}`} value={i}>{armorLabel(a)}</option>
    ))
    const fatigues = Fatigues.map((f, i) => (
      <option key={f.name} value={i}>{f.name}</option>
    ))
    const base = 5;
    const threshold = base + this.state.size
    const light = threshold
    const medium = threshold * 2
    const heavy = threshold * 3
    const incap = threshold * 4
    const dead = incap + 1
    return (
      <Container>
        <Menu compact>
          <Dropdown pointing icon='wrench' text="Tools">
            <Dropdown.Menu>
              <Dropdown.Header>Browser Storage</Dropdown.Header>
              <Dropdown.Item onClick={this.onSave.bind(this)}>Save</Dropdown.Item>
              <Dropdown.Item>
                <Dropdown text='Load'>
                  <Dropdown.Menu>
                    <Dropdown.Header>Saved Characters</Dropdown.Header>
                    {names}
                  </Dropdown.Menu>
                </Dropdown>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Header>External Storage</Dropdown.Header>
              <Dropdown.Item onClick={() => this.importLink.current.click()}>Import</Dropdown.Item>
              <Dropdown.Item onClick={this.onExport.bind(this)}>Export</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Header>Irreversable Actions!</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState(DefaultCharacter)}>Reset</Dropdown.Item>
              <Dropdown.Item onClick={() => localStorage.clear()}>Clear All Saved</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Menu>
        <a ref={this.exportLink} style={{ display: "none" }}></a>
        <input ref={this.importLink} type="file" hidden onChange={this.onImport.bind(this)} />
        <Segment vertical className="xstripe">
          <Input label='Name:' defaultValue={this.state.name} onChange={this.onChangeName.bind(this)}></Input>
          {/*<Select placeholder="Select character" options={getSavedNames()} />*/}
        </Segment>
        {/* Characteristics */}
        <Segment vertical className="xstripe">
          <Grid>{characteristics}</Grid>
          <Input label="Size" defaultValue={this.state.size} type="number" min="-4" step="1" onChange={this.onChangeSize.bind(this)}></Input>
        </Segment>
        {/* Abilities */}
        <Segment vertical className="xstripe"><Grid>{abilities}</Grid></Segment>
        {/* Armor */}
        <Segment vertical className="xstripe">
          <Item.Group>
            <Item>
              <Item.Content>
                <Item.Header>Armor</Item.Header>
                <Item.Meta>Select armor that material and coverage of the predominate material.</Item.Meta>
                <Item.Description>
                  <select name="Armor"
                    onChange={this.onChangeArmor.bind(this)}
                    value={this.state.armor}>
                    {armors}
                  </select>
                </Item.Description>
              </Item.Content>
            </Item>
            <Item>
              <Item.Content>
                <Item.Header>Soak</Item.Header>
                <Item.Description>
                  <span>{soak(this.state)}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                  <Button toggle active={this.state.tough} onClick={this.onTough.bind(this)}>Tough Virtue</Button>
                </Item.Description>
              </Item.Content>
            </Item>
            <Item>
              <Item.Content>
                <Item.Header>Load</Item.Header>
                <Item.Description>{load(this.state)}</Item.Description>
              </Item.Content>
            </Item>
            <Item>
              <Item.Content>
                <Item.Header>Encumbrance</Item.Header>
                <Item.Description>{encumbrance(this.state)}</Item.Description>
              </Item.Content>
            </Item>
            <Item>
              <Item.Content>
                <Item.Header>Fatigue</Item.Header>
                <Item.Description>
                  <select name="fatigue" onChange={this.onChangeFatigue.bind(this)} value={this.state.fatigue}>
                    {fatigues}
                  </select>
                </Item.Description>
              </Item.Content>
            </Item>
            <Item>
              <Table>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Wounds</Table.HeaderCell>
                    <Table.HeaderCell>Range</Table.HeaderCell>
                    <Table.HeaderCell>Number</Table.HeaderCell>
                    <Table.HeaderCell>Penalty</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell>Light Wounds</Table.Cell>
                    <Table.Cell>1-{light}</Table.Cell>
                    <Table.Cell><input type="number" min="0" step="1" onChange={this.onChangeLight.bind(this)}></input></Table.Cell>
                    <Table.Cell>-1</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Medium Wounds</Table.Cell>
                    <Table.Cell>{light + 1}-{medium}</Table.Cell>
                    <Table.Cell><input type="number" min="0" step="1" onChange={this.onChangeMedium.bind(this)}></input></Table.Cell>
                    <Table.Cell>-3</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Heavy Wounds</Table.Cell>
                    <Table.Cell>{medium + 1}-{heavy}</Table.Cell>
                    <Table.Cell><input type="number" min="0" step="1" onChange={this.onChangeHeavy.bind(this)}></input></Table.Cell>
                    <Table.Cell>-5</Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Incapacitated</Table.Cell>
                    <Table.Cell>{heavy + 1}-{incap}</Table.Cell>
                    <Table.Cell><input type="checkbox" onChange={this.onChangeIncap.bind(this)}></input></Table.Cell>
                    <Table.Cell></Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>Dead</Table.Cell>
                    <Table.Cell>{dead}+</Table.Cell>
                    <Table.Cell><input type="checkbox" onChange={this.onChangeDead.bind(this)}></input></Table.Cell>
                    <Table.Cell></Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            </Item>
          </Item.Group>
        </Segment>
        {/* Weapons */}
        <Segment vertical className="xstripe">
          <Checkbox onChange={this.onChangeShowSelectWeapons.bind(this)} label="Show Equiped Weapons Only" checked={this.state.showSelectedWeapons}></Checkbox>
          <table>
            <colgroup>
              <col className="weapon-col"></col>
            </colgroup>
            <thead>
              <tr>
                <th>Weapon</th>
                <th>Init</th>
                <th>Atk</th>
                <th>Def</th>
                <th>Dmg</th>
                <th>Load</th>
                <th>Range</th>
              </tr>
            </thead>
            <tbody>{weapons}</tbody>
          </table>
        </Segment>
      </Container>
    );
  }
}
