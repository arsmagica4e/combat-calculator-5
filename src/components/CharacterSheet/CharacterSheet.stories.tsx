import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import CharacterSheet from "./CharacterSheet";

/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const CharacterSheetReadme = require("./README.md");

storiesOf("CharacterSheet", module)
  .addDecorator(withReadme(CharacterSheetReadme))
  .add("default", () => (
    <CharacterSheet />
  ));
