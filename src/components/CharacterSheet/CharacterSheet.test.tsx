import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import CharacterSheet from "./CharacterSheet";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("CharacterSheet component", () => {
  it("should render correctly", () => {
    const wrapper = render(<CharacterSheet />);
    expect(wrapper).toMatchSnapshot();
  });
});
